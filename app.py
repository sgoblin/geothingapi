# Python geothingAPI
import falcon
from geoip2 import database as geodb
import geoip2
import socket
import getWhois
import json
from jinja2 import Template
from hirlite import Rlite
from urllib.parse import urlparse, unquote_plus

reader = geodb.Reader('GeoLite2-City.mmdb')
with open('index.html.j2', 'r') as theFile:
    rawIndex = theFile.read()
indexTemplate = Template(rawIndex)
cacheDB = Rlite(encoding='utf8', path='cachegeo.rld')

def qToHostname(q):
    decodedQ = unquote_plus(unquote_plus(unquote_plus(q.strip()))).lower()
    if (decodedQ[:7] == 'http://' or decodedQ[:8] == 'https://' or (not('/' in decodedQ))):
        theHostName = urlparse(decodedQ).hostname
    elif (not(decodedQ[:7] == 'http://' or decodedQ[:8] == 'https://') and ('/' in decodedQ)):
        theHostName = decodedQ.split('/')[0]
    
    if (theHostName == None):
        theHostName = decodedQ
    return theHostName

def hostnameToIPDomain(theHostName, req):
    current = {}

    try:
        socket.inet_pton(socket.AF_INET, theHostName)
        current['ip'] = theHostName
        try:
            current['domain'] = socket.gethostbyaddr(current['ip'])[0]
        except socket.herror:
            current['domain'] = "Unknown"
    except OSError:
        try:
            socket.inet_pton(socket.AF_INET6, theHostName)
            current['ip'] = theHostName
            try:
                current['domain'] = socket.gethostbyaddr(current['ip'])[0]
            except socket.herror:
                current['domain'] = "Unknown"
        except OSError:
            #print('oh no, we got a second oserror')
            current['domain'] =  theHostName
            #print(current['domain'])
            current['ip'] = socket.gethostbyname(current['domain'])

    if (req.get_param('domain')):
        current['domain'] = req.get_param('domain').strip()
    
    return current

class LookupResource(object):
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        current = {}

        resptype = req.get_param('type')

        try:

            # Start ip finding

            if (req.get_param('q')):
                queryBool = True

                theHostName = qToHostname(req.get_param('q'))

                if (theHostName == None):
                    theHostName = req.get_param('q')
                print(theHostName)
                current = hostnameToIPDomain(theHostName, req)
                
            else:
                current['ip'] = req.access_route[0]
                queryBool = False
                try:
                    current['domain'] = socket.gethostbyaddr(current['ip'])[0]
                except socket.herror:
                    current['domain'] = 'Unknown'
                print(current['domain'])


            '''if (req.get_param('ip') or req.get_param('domain')):
                if (req.get_param('ip')):
                    current['ip'] = req.get_param('ip').strip()
                    try:
                        current['domain'] = socket.gethostbyaddr(current['ip'])[0]
                    except socket.herror:
                        current['domain'] = "Unknown"
                else:
                    current['domain'] = req.get_param('domain').strip()
                    print(current['domain'])
                    current['ip'] = socket.gethostbyname(current['domain'])
            else:
                current['ip'] = req.access_route[0]
                current['domain'] = socket.gethostbyaddr(current['ip'])[0]
                print(current['domain'])'''

            # End ip finding
            # Start retrieval of data
            dbResponse = cacheDB.command('get', current['ip'])
            if (dbResponse):
                theDomain = current['domain']
                current = json.loads(dbResponse)
                current['domain'] = theDomain
                print('Got response from cache')
                cachedResponseBool = True
            else:
                try:
                    currentGeoInfo = reader.city(current['ip'])
                    current['city'] = currentGeoInfo.city.name
                    current['country'] = currentGeoInfo.country.name
                    current['continent'] = currentGeoInfo.continent.name
                    current['latitude'] = currentGeoInfo.location.latitude
                    current['longitude'] = currentGeoInfo.location.longitude
                except geoip2.errors.AddressNotFoundError:
                    current['city'] = "Unknown or Private"
                    current['country'] = "Unknown or Private"
                    current['continent'] = "Unknown or Private"
                    current['latitude'] = 0
                    current['longitude'] = 0
                current['org'] = getWhois.orgname(current['ip'])

                try:
                    print(current['org'] + "HI!")
                    #print("NONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\nNONETYPE\n")
                    print(current['ip'])
                    print(type(current['org']))
                except TypeError:
                    current['org'] = "Unknown"

                for key in current:
                    if (current[key] is None):
                        current[key] = 'Unknown'
                print('Got response from original sources')
                cachedResponseBool = False

            # End retrieve data
            # Start caching
            if (not(cachedResponseBool) or resptype == 'json'):
                jsonResponse = json.dumps(current)
            if not(cachedResponseBool):
                cacheDB.command('set', current['ip'], jsonResponse)
                cacheDB.command('expire', current['ip'], '5259600')
            # End caching

        except socket.gaierror:
            #print('gaierror')
            current = {'country': 'Unknown', 'domain': 'Unknown', 'ip': 'Unknown', 'org': 'Unknown', 'latitude': 0, 'city': 'Unknown', 'longitude': 0, 'continent': 'Unknown'}
            jsonResponse = json.dumps(current)
        except KeyError:
            print('Well, that\'s weird!')

        # Processing the responses into desired response format

        if (resptype == "text"):
            resp.body = ("Domain: " + current['domain'] +
                        "\nIP: " + current['ip'] +
                        "\nCity: " + current['city'] +
                        "\nCountry: " + current['country'] +
                        "\nContinent: " + current['continent'] +
                        "\nOrganization: " + current['org'])
        elif (resptype == "json"):
            resp.content_type = "application/json"
            '''if (resp.get_header("Referer")):
                parsedReferer = urlparse(req.get_header("Referer"))
                resp.set_header("Access-Control-Allow-Origin", (parsedReferer.scheme + parsedReferer.hostname))'''
            resp.set_header("Access-Control-Allow-Origin", "*")
            resp.body = jsonResponse
        else:
            resp.content_type = "text/html"
            resp.body = indexTemplate.render(latitude=int(current['latitude']),
                                            longitude=int(current['longitude']),
                                            domain=current['domain'],
                                            ip=current['ip'],
                                            city=current['city'],
                                            country=current['country'],
                                            continent=current['continent'],
                                            org=current['org'])

        # End response processing
        #print('Success! Let\'s celebrate!')

class GetIPResource(object):
    def on_get(self, req, resp):
        resp.body = json.dumps({'ip': req.access_route[0]})
        resp.content_type = "application/json"
        resp.set_header("Access-Control-Allow-Origin", "*")
        resp.status = falcon.HTTP_200

app = falcon.API()

lookup = LookupResource()
getIP = GetIPResource()

app.add_route('/', lookup)
app.add_route('/ip', getIP)
