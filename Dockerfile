FROM alpine:3.5

RUN apk update && apk upgrade && apk add git python3 wget whois && mkdir /var/www/ && mkdir /var/www/geoip
WORKDIR /var/www/geoip
RUN git clone https://bitbucket.org/sgoblin/geothingapi.git ./
RUN sh setup.sh
EXPOSE 8000
CMD ./gEnv/bin/gunicorn -b 0.0.0.0:8000 app:app
