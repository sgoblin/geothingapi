#!/usr/bin/env sh
wget https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz
gunzip GeoLite2-City.mmdb.gz
pip3 install -U virtualenv
virtualenv -p python3 gEnv
. gEnv/bin/activate
pip install -Ur requirements.txt