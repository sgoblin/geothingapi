import subprocess

def g(ipaddr):
    whoisStrList = subprocess.run(['whois', ipaddr], stdout=subprocess.PIPE, universal_newlines=True).stdout.strip().split("\n")
    #print(whoisStrList)
    for x in range(200):
        try:
            for i in range(len(whoisStrList)):
                if(whoisStrList[i] == ''):
                    whoisStrList.remove(whoisStrList[i])
                else:
                    if (whoisStrList[i][0] == '#'):
                        whoisStrList.remove(whoisStrList[i])
        except IndexError:
            pass

    whoisSplitList = []
    for i in whoisStrList:
        whoisSplitList.append(i.strip().split(':'))

    whoisList = {}
    for y in range(20):
        for i in whoisSplitList:
            try:
                temp = []
                for x in i:
                    temp.append(x.strip())
                try:
                    whoisList[temp[0]]
                except KeyError:
                    whoisList[temp[0]] = temp[1]
            except IndexError:
                #print("IndexError. EEEEEEEEEEEEEEEEK!")
                pass

    return whoisList

def orgname(ipaddr):
    whoisList = g(ipaddr)
    try:
        return whoisList["OrgName"]
    except KeyError:
        try:
            return whoisList["org-name"]
        except KeyError:
            try:
                print("using descr for orgname")
                return whoisList["descr"]
            except:
                try:
                    print("using role for orgname")
                    return whoisList["role"]
                except:
                    try:
                        print("using netname for orgname")
                        return whoisList["netname"]
                    except KeyError:
                        return "Unknown"
